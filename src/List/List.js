import React from 'react';
import { ListItem } from './ListItem';
import { data as items } from './data';
import { capitalizeFirstLetter as capitalize } from '../utils/helpers';

export const List = () => {
  const puntos = items.map(el => (
    <ListItem title={capitalize(el)} key={items.indexOf(el)} />
  ));
  return <div>{puntos}</div>;
};
