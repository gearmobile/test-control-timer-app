import React from 'react';

export const ListItem = ({ title }) => {
  return (
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <div className="input-group-text">
          <input type="checkbox" defaultChecked="true" />
        </div>
      </div>
      <div className="form-control">{title}</div>
    </div>
  );
};
