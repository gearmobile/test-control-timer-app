import React, { PureComponent } from 'react';

// LIBRARIES
import spacetime from 'spacetime';

// COMPONENTS
import { List } from './List/List';

export default class App extends PureComponent {
  state = {
    currDate: spacetime.today(),
    timezone: spacetime.today().timezone().name,
    startPoint: spacetime(1548972000000),
    endPoint: spacetime(1551391200000)
  };

  getTime = () => {};
  showCurrentDate = () => {
    const { currDate: current } = this.state;
    return `${current.dayName()} ${current.date()} ${current.monthName()} ${current.year()}`;
  };
  showNextDate = () => {
    this.setState(({ currDate }) => {
      return {
        currDate: currDate.next('date')
      };
    });
  };
  showPrevDate = () => {
    this.setState(({ currDate }) => {
      return {
        currDate: currDate.last('date')
      };
    });
    this.setPrevButonStatus();
  };
  setCurrentDate = () => {
    this.setState({
      currDate: spacetime.today()
    });
  };
  setButtonStatus = () => {
    const { currDate } = this.state;
    return currDate.epoch === spacetime.today().epoch;
  };
  setInputStatus = () => {
    const active = {
      borderColor: 'salmon',
      borderWidth: '2px'
    };
    const notActive = {
      borderColor: '#ced4da',
      borderWidth: '1px'
    };
    return this.setButtonStatus() ? active : notActive;
  };
  setNextButtonStatus = () => {
    const { currDate, endPoint } = this.state;
    return currDate.epoch === endPoint.epoch;
  };
  setPrevButonStatus = () => {
    const { currDate, startPoint } = this.state;
    return currDate.epoch === startPoint.epoch;
  };
  render() {
    const currDate = this.showCurrentDate();
    const { timezone } = this.state;
    const btnStatus = this.setButtonStatus();
    const intStatus = this.setInputStatus();
    const nextButtonStatus = this.setNextButtonStatus();
    const prevButonStatus = this.setPrevButonStatus();
    return (
      <div className="container">
        <div className="row mt-4">
          <div className="col-md-10 offset-1">
            <h3 className="text-center text-capitalize text-success mb-4">
              example
            </h3>
            <div className="d-flex justify-content-center align-items-center">
              <button
                onClick={this.setCurrentDate}
                type="button"
                className="btn btn-outline-info mr-1"
                disabled={btnStatus}
              >
                TODAY
              </button>
              <button
                onClick={this.showPrevDate}
                type="button"
                className="btn btn-outline-dark mr-1"
                disabled={prevButonStatus}
              >
                <i className="fa fa-chevron-left" />
              </button>
              <input
                type="text"
                className="form-control text-capitalize font-weight-bolder mr-1"
                style={intStatus}
                id="search"
                onChange={this.getTime}
                value={currDate}
                placeholder="text"
              />
              <button
                onClick={this.showNextDate}
                type="button"
                className="btn btn-outline-dark"
                disabled={nextButtonStatus}
              >
                <i className="fa fa-chevron-right" />
              </button>
              <div className="pl-2">{timezone}</div>
            </div>
          </div>
        </div>
        <div className="row mt-4">
          <div className="col-md-10 offset-md-1">
            <h3 className="text-center text-capitalize text-info">checklist</h3>
            <List />
          </div>
        </div>
      </div>
    );
  }
}
